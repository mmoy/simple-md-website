# simple-md-website

Very simple HTML site generator from markdown files.

Based on https://jez.io/pandoc-markdown-css-theme/, but much lighter (just 2 CSS files, one HTML template).
